FROM jenkins/jenkins:jdk11

EXPOSE 8080
EXPOSE 50000

USER root

RUN apt-get update
RUN apt-get install -y isc-dhcp-server
RUN mv /usr/sbin/dhcpd /bin/dhcpd

RUN curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
RUN chmod 774 /usr/local/bin/docker-compose

USER jenkins